require('dotenv').config();
const express = require('express');
const http = require('http');
const cors = require('cors');
const mongoose = require('mongoose');

const socketServer = require('./socketServer');

const authRoutes = require('./routes/authRoutes');
const friendInvitationRoutes = require('./routes/friendInvitationRoutes');

const PORT = parseInt(process.env.API_PORT, 10) || 5002;

const app = express();
app.use(express.json());
app.use(cors());

/** register routs */
app.use('/api/auth', authRoutes);
app.use('/api/friend-invitation', friendInvitationRoutes);

const server = http.createServer(app);
socketServer.registerSocketServer(server);

async function connectDb() {
  try {
    mongoose.set('strictQuery', false);
    await mongoose.connect(process.env.MONGO_URI, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      autoIndex: true,
    });
    console.log('Mongodb connected');
  } catch (error) {
    console.log(error);
    process.exit(1);
  }
}

connectDb()
  .then(() => {
    server.listen(PORT, () => {
      console.log(`Server is listening on ${PORT}`);
    });
  })
  .catch((error) => {
    console.log(`Database connection failed. Server not started.`);
    console.log(error);
  });
