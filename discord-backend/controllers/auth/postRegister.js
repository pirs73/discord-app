const jwt = require('jsonwebtoken');
const User = require('../../models/user');

const postRegister = async (req, res) => {
  try {
    const { mail, username, password } = req.body;

    /** check if user exists */
    const userExists = await User.exists({ mail: mail.toLowerCase() });

    if (userExists) {
      return res.status(409).send('E-mail already in use.');
    }

    /** create user document and save in database */
    const user = await User.create({
      mail: mail.toLowerCase(),
      username,
      password,
    });

    /** create JWT token */
    const token = jwt.sign(
      {
        userId: user._id,
        mail,
      },
      process.env.TOKEN_KEY,
      {
        expiresIn: '24h',
      },
    );

    res.status(201).json({
      userDetails: {
        mail: user.mail,
        username: user.username,
        token,
      },
    });
  } catch (error) {
    console.error(error);
    return res.status(500).send('Error occured. Please try again.');
  }
};

module.exports = postRegister;
