const mongoose = require('mongoose');
// const bcrypt = require('bcryptjs');
const { Schema } = mongoose;

const userSchema = new Schema({
  mail: { type: String, unique: true },
  username: { type: String },
  password: {
    type: String,
    required: true,
  },
  friends: [{ type: Schema.Types.Object, ref: 'User' }],
});

// userSchema.pre('save', function (next) {
//   const user = this;

//   bcrypt.genSalt(10, function (err, salt) {
//     if (err) {
//       return next(err);
//     }

//     bcrypt.hash(user.password, salt, function (err, hash) {
//       if (err) {
//         return next(err);
//       }

//       user.password = hash;
//       next();
//     });
//   });
// });

// userSchema.methods.validatePassword = function (candidatePassword, done) {
//   bcrypt.compare(candidatePassword, this.password, function (error, isSuccess) {
//     if (error) {
//       return done(error);
//     }

//     return done(null, isSuccess);
//   });
// };

module.exports = mongoose.model('User', userSchema);
