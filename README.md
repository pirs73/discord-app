# Discord App

## Getting started

## Installation Database

```bash
$ docker-compose up -d
or
$ podman-compose up -d
```

## Start server

```bash
$ cd discord-backend
$ npm i
$ npm run seed
$ npm run start
```

## Frontend

```bash
$ cd discord-frontend
$ npm i
$ npm run start
```

## Backup DB

```bash
$ mongodump --db=discord --uri="mongodb://127.0.0.1:27017"
$ mongorestore --drop  dump/
windows
.\mongodump --db=discord --uri="mongodb://127.0.0.1:27017"
.\mongorestore --drop  dump/
```
