import React from 'react';
import { Navigate, Routes, Route } from 'react-router-dom';
import { LoginPage, RegisterPage } from './authPages';
import { Dashboard } from './Dashboard';
import { AlertNotification } from './shared/components';
import './App.css';

function App() {
  return (
    <>
      <Routes>
        <Route end path="/login" element={<LoginPage />} />
        <Route end path="/register" element={<RegisterPage />} />
        <Route end path="/dashboard" element={<Dashboard />} />
        <Route path="/" element={<Navigate to="/dashboard" replace />} />
      </Routes>
      <AlertNotification />
    </>
  );
}

export default App;
