/* eslint-disable no-useless-escape */
const validatePassword = (password) => {
  return password && password.length >= 6 && password.length <= 12;
};

export const validateMail = (mail) => {
  const emailPattern =
    /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  // const emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;

  return emailPattern.test(mail);
};

const validateUsername = (username) => {
  return username && username.length >= 2 && username.length <= 64;
};

export const validateLoginForm = ({ mail, password }) => {
  return validateMail(mail) && validatePassword(password);
};

export const validateRegisterForm = ({ mail, username, password }) => {
  return (
    validateMail(mail) &&
    validateUsername(username) &&
    validatePassword(password)
  );
};
