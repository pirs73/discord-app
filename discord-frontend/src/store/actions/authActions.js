import * as api from '../../api';
import { openAlertMessage } from './alertActions';

export const authActions = {
  SET_USER_DETAILS: 'AUTH.SET_USER_DETAILS',
};

const setUserDetails = (userDetails) => {
  return {
    type: authActions.SET_USER_DETAILS,
    userDetails,
  };
};

const login = (userDetails, navigate) => {
  return async (dispatch) => {
    const response = await api.login(userDetails);

    if (response.error) {
      /** show error message in alert */
      dispatch(openAlertMessage(response?.exception?.response?.data));
    } else {
      const { userDetails } = response?.data;
      localStorage.setItem('user', JSON.stringify(userDetails));

      dispatch(setUserDetails(userDetails));
      navigate('/dashboard');
    }
  };
};

const register = (userDetails, navigate) => {
  return async (dispatch) => {
    const response = await api.register(userDetails);

    if (response.error) {
      /** show error message in alert */
      dispatch(openAlertMessage(response?.exception?.response?.data));
    } else {
      const { userDetails } = response?.data;
      localStorage.setItem('user', JSON.stringify(userDetails));
      // if (process.env.NODE_ENV === 'production') {
      //   Cookies.set('user', JSON.stringify(userDetails), {
      //     expires: 1,
      //     secure: true,
      //     sameSite: 'strict',
      //     domain: process.env.DOMAIN,
      //   });
      // } else {
      // Cookies.set('user', JSON.stringify(userDetails), {
      //   sameSite: 'lax',
      //   expires: 7,
      // });
      // }

      dispatch(setUserDetails(userDetails));
      navigate('/dashboard');
    }
  };
};

export const getActions = (dispatch) => {
  return {
    login: (userDetails, navigate) => dispatch(login(userDetails, navigate)),
    register: (userDetails, navigate) =>
      dispatch(register(userDetails, navigate)),
    setUserDetails: (userDetails) => dispatch(setUserDetails(userDetails)),
  };
};
