import React from 'react';
import { styled } from '@mui/system';
import { MainPageButton } from './MainPageButton';

const MainContainer = styled('div')({
  display: 'flex',
  flexFlow: 'column nowrap',
  alignItems: 'center',
  width: '72px',
  height: '100%',
  backgroundColor: '#202225',
});

const SideBar = () => {
  return (
    <MainContainer>
      <MainPageButton />
    </MainContainer>
  );
};

export { SideBar };
