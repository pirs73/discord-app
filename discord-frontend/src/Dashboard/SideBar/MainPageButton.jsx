import React from 'react';
import { Button } from '@mui/material';
import GroupsIcon from '@mui/icons-material/Groups';

const MainPageButton = () => {
  return (
    <Button
      style={{
        width: '48px',
        height: '48px',
        borderRadius: '16px',
        margin: '10px 0 0 0',
        paddind: 0,
        color: '#fff',
        backgroundColor: '#5865f2',
      }}
    >
      <GroupsIcon />
    </Button>
  );
};

export { MainPageButton };
