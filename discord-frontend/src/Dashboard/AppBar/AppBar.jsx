import React from 'react';
import { styled } from '@mui/system';
import { DropdownMenu } from './DropdownMenu';

const MainContainer = styled('div')({
  position: 'absolute',
  display: 'flex',
  justifyContent: 'space-between',
  alignItems: 'center',
  top: '0',
  right: '0',
  height: '48px',
  borderBottom: '1px solid #000',
  backgroundColor: '#36393f',
  width: 'calc(100% - 326px)',
  padding: '0 15px',
});

const AppBar = () => {
  return (
    <MainContainer>
      <DropdownMenu />
    </MainContainer>
  );
};

export { AppBar };
