import React from 'react';
import { Box } from '@mui/material';
import { FiberManualRecord as FiberManualRecordIcon } from '@mui/icons-material';

const OnlineIndicator = () => {
  return (
    <Box
      sx={{
        position: 'absolute',
        display: 'flex',
        alignItems: 'center',
        color: '#3ba55d',
        right: '5px',
      }}
    >
      <FiberManualRecordIcon />
    </Box>
  );
};

export { OnlineIndicator };
