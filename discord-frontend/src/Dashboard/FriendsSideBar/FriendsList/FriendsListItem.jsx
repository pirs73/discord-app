import React from 'react';
import { Button, Typography } from '@mui/material';
import { Avatar } from '../../../shared/components';
import { OnlineIndicator } from './OnlineIndicator';

const FriendsListItem = ({ id, username, isOnline }) => {
  return (
    <Button
      style={{
        position: 'relative',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-start',
        width: '100%',
        height: '42px',
        marginTop: '10px',
        textTransform: 'none',
        color: '#000',
      }}
    >
      <Avatar username={username} />
      <Typography
        style={{ marginLeft: '7px', fontWeight: '700', color: '#8e9297' }}
        variant="subtitle1"
        align="left"
      >
        {username}
      </Typography>
      {isOnline && <OnlineIndicator />}
    </Button>
  );
};

export default FriendsListItem;
