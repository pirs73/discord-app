import React from 'react';
import { styled } from '@mui/system';

const MainContainer = styled('div')({
  display: 'flex',
  flexGrow: 1,
  backgroundColor: '#36393f',
  marginTop: '48px',
});

const Messenger = () => {
  return <MainContainer>Messenger</MainContainer>;
};

export { Messenger };
